from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/', methods=['GET'])
def get_index():
    #TODO Implement index page
    return render_template('index.html')

@app.route('/', methods=['POST'])
def post_index():

    input = request.form['input']

    output = ''.join([oodlify(x) for x in input])

    return render_template('index.html', input=input, output=output)

def oodlify(char):
    vowels = ['a','e','i','o','u']

    if char.lower() in vowels:
        return 'oodle'
    else:
        return char

